#include "../src/game.h"
#include "../src/gameUtils.h"
#include <gtest/gtest.h>

void canvasChanges(const std::string &_) {

}

class GameTestsLines : public ::testing::TestWithParam<std::vector<std::tuple<int, int>>> {
};

TEST_P(GameTestsLines, testShouldPlayerWinLine) {
    tic_tac_toe::Game game(canvasChanges);
    auto source = GetParam();
    for (auto i = 0; i < source.size(); i++)
        if (i % 2 == 0)
            game.playerMove(std::get<0>(source[i]), std::get<1>(source[i]));
        else
            game.aiMove(std::get<0>(source[i]), std::get<1>(source[i]));

    EXPECT_TRUE(game.isGameOver());
    EXPECT_EQ(tic_tac_toe::playerCage, game.getWinner());
}

INSTANTIATE_TEST_CASE_P

(PlayerWinTestsLines,
 GameTestsLines,
 ::testing::Values(
         std::vector<std::tuple<int, int>>{ // 1 горизонтальная линия
                 std::make_tuple(0, 0),
                 std::make_tuple(0, 2),
                 std::make_tuple(1, 0),
                 std::make_tuple(1, 2),
                 std::make_tuple(2, 0)
         },
         std::vector<std::tuple<int, int>>{// 2 горизонтальная линия
                 std::make_tuple(0, 1),
                 std::make_tuple(0, 2),
                 std::make_tuple(1, 1),
                 std::make_tuple(1, 2),
                 std::make_tuple(2, 1)
         },
         std::vector<std::tuple<int, int>>{ // 3 горизонтальная линия
                 std::make_tuple(0, 2),
                 std::make_tuple(0, 0),
                 std::make_tuple(1, 2),
                 std::make_tuple(1, 1),
                 std::make_tuple(2, 2)
         },

         std::vector<std::tuple<int, int>>{ // 1 вертикальная линия
                 std::make_tuple(0, 0),
                 std::make_tuple(2, 0),
                 std::make_tuple(0, 1),
                 std::make_tuple(2, 1),
                 std::make_tuple(0, 2)
         },
         std::vector<std::tuple<int, int>>{// 2 вертикальная линия
                 std::make_tuple(1, 0),
                 std::make_tuple(0, 2),
                 std::make_tuple(1, 1),
                 std::make_tuple(0, 1),
                 std::make_tuple(1, 2)
         },
         std::vector<std::tuple<int, int>>{ // 3 вертикальная линия
                 std::make_tuple(2, 0),
                 std::make_tuple(0, 0),
                 std::make_tuple(2, 1),
                 std::make_tuple(1, 1),
                 std::make_tuple(2, 2)
         },

         std::vector<std::tuple<int, int>>{ // диагональ правая
                 std::make_tuple(0, 0),
                 std::make_tuple(0, 2),
                 std::make_tuple(1, 1),
                 std::make_tuple(0, 1),
                 std::make_tuple(2, 2)
         },
         std::vector<std::tuple<int, int>>{// диагональ левая
                 std::make_tuple(2, 0),
                 std::make_tuple(2, 1),
                 std::make_tuple(1, 1),
                 std::make_tuple(0, 1),
                 std::make_tuple(0, 2)
         }));

TEST(GameTests, testShouldDrawOnFill) {
    tic_tac_toe::Game game(canvasChanges);

    game.playerMove(0, 0);
    game.aiMove(1, 0);
    game.playerMove(2, 0);
    game.aiMove(0, 1);
    game.playerMove(1, 1);
    game.aiMove(2, 2);
    game.playerMove(2, 1);
    game.aiMove(0, 2);
    game.playerMove(1, 2);

    EXPECT_TRUE(game.isGameOver());
    EXPECT_EQ(tic_tac_toe::empty, game.getWinner());
}

TEST(GameTests, testShouldNotMoveIfCageExists) {
    tic_tac_toe::Game game(canvasChanges);
    game.playerMove(1, 2);
    EXPECT_FALSE(game.aiMove(1, 2));
    EXPECT_FALSE(game.playerMove(1, 2));
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}