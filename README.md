# Крестики-нолики

[![pipeline status](https://gitlab.com/kotC9/rostictactoe/badges/master/pipeline.svg)](https://gitlab.com/kotC9/rostictactoe/-/commits/master)

## Общее
Тестировалось на ros melodic, размером доски 3x3

На Windows 10, docker и manjaro

## Сборка
```shell
mkdir -p catkin_ws/src           # папка catkin_ws - место сборки, src - наш package
source /opt/ros/melodic/setup.sh # на windows: call c:\opt\ros\melodic\x64\setup.bat
cd catkin_ws/src
git clone https://gitlab.com/kotC9/rostictactoe.git
cd .. 
catkin_make
source ./devel/setup.sh          # для команд rosrun/roslaunch tic_tac_toe
```

## Запуск: (в разных терминалах)

"Серверная" часть:
```
rosrun tic_tac_toe ai
rosrun tic_tac_toe board
```

Либо:
```
roslaunch tic_tac_toe server.launch
```

Запросы: 

* `rosrun tic_tac_toe player X Y` ход игрока, X,Y - координаты, uint32
* `rosrun tic_tac_toe restart` перезапуск игры

## Узлы

Используется 3 узла:

1. `board` - доска для игры, принимает запросы от `player` и `ai`
2. `player` - сервис пользователь, отправляет ход на доску, параметры X Y
3. `ai` - action искуственный интеллект, асинхронно отправляет ответ на доску. 
Считается, что ответ от ИИ может быть долгим, поэтому используется action вместо service. Связь предствлена ниже.
![](graph.png)

## Unit tests

Для тестирования используется `gtest`

Запуск тестов:
```
catkin_make run_tests
```

## Контакты

Разработчик - svssys@ya.ru Сергей Соколов
