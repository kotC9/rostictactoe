// https://www.geeksforgeeks.org/minimax-algorithm-in-game-theory-set-3-tic-tac-toe-ai-finding-optimal-move/

#include "AiAlgo.h"

bool MinimaxAiAlgo::isMovesLeft(std::vector<std::vector<tic_tac_toe::cageState>> board) {
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            if (board[i][j] == tic_tac_toe::empty)
                return true;
    return false;
}

int MinimaxAiAlgo::evaluate(std::vector<std::vector<tic_tac_toe::cageState>> b) {
    for (int row = 0; row < 3; row++) {
        if (b[row][0] == b[row][1] &&
            b[row][1] == b[row][2]) {
            if (b[row][0] == tic_tac_toe::aiCage)
                return +10;
            else if (b[row][0] == tic_tac_toe::playerCage)
                return -10;
        }
    }

    for (int col = 0; col < 3; col++) {
        if (b[0][col] == b[1][col] &&
            b[1][col] == b[2][col]) {
            if (b[0][col] == tic_tac_toe::aiCage)
                return +10;

            else if (b[0][col] == tic_tac_toe::playerCage)
                return -10;
        }
    }

    if (b[0][0] == b[1][1] && b[1][1] == b[2][2]) {
        if (b[0][0] == tic_tac_toe::aiCage)
            return +10;
        else if (b[0][0] == tic_tac_toe::playerCage)
            return -10;
    }

    if (b[0][2] == b[1][1] && b[1][1] == b[2][0]) {
        if (b[0][2] == tic_tac_toe::aiCage)
            return +10;
        else if (b[0][2] == tic_tac_toe::playerCage)
            return -10;
    }

    return 0;
}

int MinimaxAiAlgo::minimax(std::vector<std::vector<tic_tac_toe::cageState>> board, int depth, bool isMax) {
    int score = evaluate(board);

    if (score == 10)
        return score;

    if (score == -10)
        return score;

    if (!isMovesLeft(board))
        return 0;

    if (isMax) {
        int best = -1000;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == tic_tac_toe::empty) {
                    board[i][j] = tic_tac_toe::aiCage;

                    best = std::max(best,
                                    minimax(board, depth + 1, !isMax));

                    board[i][j] = tic_tac_toe::empty;
                }
            }
        }
        return best;
    } else {
        int best = 1000;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == tic_tac_toe::empty) {
                    board[i][j] = tic_tac_toe::playerCage;

                    best = std::min(best,
                                    minimax(board, depth + 1, !isMax));

                    board[i][j] = tic_tac_toe::empty;
                }
            }
        }
        return best;
    }
}


tic_tac_toe::AiMoveResult MinimaxAiAlgo::AiChooseCage(std::vector<std::vector<tic_tac_toe::cageState>> state) {
    int x, y;
    tic_tac_toe::AiMoveResult res;

    int bestVal = -1000;
    res.cageX = -1;
    res.cageY = -1;

    // Traverse all cells, evaluate minimax function for
    // all empty cells. And return the cell with optimal
    // value.
    for (int i = 0; i<3; i++)
    {
        for (int j = 0; j<3; j++)
        {
            // Check if cell is empty
            if (state[i][j]== tic_tac_toe::empty)
            {
                // Make the move
                state[i][j] = tic_tac_toe::aiCage;

                // compute evaluation function for this
                // move.
                int moveVal = minimax(state, 0, false);

                // Undo the move
                state[i][j] = tic_tac_toe::empty;

                // If the value of the current move is
                // more than the best value, then update
                // best/
                if (moveVal > bestVal)
                {
                    res.cageX = i;
                    res.cageY = j;
                    bestVal = moveVal;
                }
            }
        }
    }

    return res;

}
