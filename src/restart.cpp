#include "ros/ros.h"
#include "tic_tac_toe/Restart.h"
#include <cstdlib>

/**
 * @brief сервис, перезапускающий игру
 * 
 * @param argc 
 * @param argv argv[1] - ширина полотна игры
 */
int main(int argc, char **argv) {
    ros::init(argc, argv, "tic_tac_toe_restart");
    if (argc != 1) {
        ROS_INFO("usage: tic_tac_toe restart");
        return 1;
    }

    tic_tac_toe::Restart srv;
    ros::NodeHandle n;
    auto restartClient = n.serviceClient<tic_tac_toe::Restart>("restart");
    if (restartClient.call(srv)) {
        ROS_INFO("[RESTART]: Restarted");
    } else {
        ROS_ERROR("[RESTART]: Failed to call service restart");
        return 1;
    }

    return 0;
}