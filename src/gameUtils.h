//
// Created by kotc9 on 15.06.22.
//

#ifndef SRC_GAMEUTILS_H
#define SRC_GAMEUTILS_H

namespace tic_tac_toe
{
    enum cageState {
        empty,
        X,
        O
    };

    const cageState playerCage = O;
    const cageState aiCage = X;
}

#endif //SRC_GAMEUTILS_H
