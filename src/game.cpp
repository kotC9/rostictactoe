#include "game.h"
using namespace tic_tac_toe;

cageState Game::checkGameOver() {
    // TODO: отрефакторить, алгоритм отстой
    cageState winner = empty;
    auto count = 0;

    // vertical
    for (auto i = 0; i < boardSize; i++) {
        cageState prev = state[i][0];
        winner = prev;
        count = 1;
        for (auto j = 1; j < boardSize; j++)
            if (state[i][j] == prev)
                count++;
        if (count == boardSize && winner != empty)
            return winner;
    }

    // horizontal
    for (auto i = 0; i < boardSize; i++) {
        cageState prev = state[0][i];
        winner = prev;
        count = 1;
        for (auto j = 1; j < boardSize; j++)
            if (state[j][i] == prev)
                count++;
        if (count == boardSize && winner != empty)
            return winner;
    }

    // diagonal lines
    cageState prev = state[0][0];
    winner = prev;
    count = 1;
    for (auto i = 1; i < boardSize; i++)
        if (state[i][i] == prev)
            count++;
    if (count == boardSize && winner != empty)
        return winner;

    prev = state[0][boardSize - 1];
    winner = prev;
    count = 1;
    for (auto i = 1; i < boardSize; i++)
        if (state[i][boardSize - 1 - i] == prev)
            count++;
    if (count == boardSize && winner != empty)
        return winner;

    return empty;
}

void Game::redraw() {
    auto drawIntermediateLine = [](int width) {
        std::string result;
        for (auto i = 0; i < width; i++) {
            result.append("----");
        }
        return result;
    };

    gameCanvas = "\n\nTic Tac Toe\nO - Player\nX - AI\n\n";
    for (auto i = 0; i < boardSize; i++) {
        for (auto j = 0; j < boardSize; j++) {
            gameCanvas.append(readableCageState[state[j][i]]);
            if (j < boardSize - 1)
                gameCanvas.append(" | ");
        }

        if (i < boardSize - 1) {
            gameCanvas.append("\n");
            gameCanvas.append(drawIntermediateLine(boardSize));
            gameCanvas.append("\n");
        }
    }
}

bool Game::tryMakeMove(uint32_t x, uint32_t y, cageState owner) {
    
    if (state[x][y] != empty)
        return false;
    state[x][y] = owner;
    currentStep++;
    return true;
}

void Game::finishGame(cageState winner) {
    gameCanvas.append("\nWinner: ");
    gameCanvas.append(winnerName[winner]);
    gameCanvas.append("\nPlease send restart to restart game\n");
    gameOver = true;
    gameWinner = winner;
}

Game::Game(void(*canvasChanged)(const std::string& canvas)) {
    this->boardSize = boardSize;
    this->canvasChanged = canvasChanged;

    maximumSteps = boardSize * boardSize;
    state.resize(boardSize);
    for (int i = 0; i < boardSize; i++)
        state[i].resize(boardSize);
    redraw();
    canvasChanged(gameCanvas);
}

bool Game::aiMove(uint32_t x, uint32_t y) {
    return doMove(x,y, aiCage);
}

bool Game::playerMove(uint32_t x, uint32_t y) {
    return doMove(x,y, playerCage);
}

bool Game::doMove(uint32_t x, uint32_t y, cageState owner) {
    if (gameOver)
        return false;

    if (!tryMakeMove(x, y, owner))
        return false;

    redraw();
    auto winner = checkGameOver();
    if (currentStep >= maximumSteps || winner != empty)
        finishGame(winner);

    canvasChanged(gameCanvas);
    return true;
}
