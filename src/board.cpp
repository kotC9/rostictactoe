
#include "ros/ros.h"
#include <actionlib/client/simple_action_client.h>
#include <tic_tac_toe/AiMoveAction.h>
#include "tic_tac_toe/PlayerMove.h"
#include "tic_tac_toe/Restart.h"
#include "game.h"

tic_tac_toe::Game *game;
actionlib::SimpleActionClient<tic_tac_toe::AiMoveAction> *aiMoveClient;

tic_tac_toe::AiMoveGoal createAiGoal() {
    tic_tac_toe::AiMoveGoal goal;
    auto count = 0;
    for (auto i = 0; i < game->getBoardSize(); ++i) {
        for (auto j = 0; j < game->getBoardSize(); ++j) {
            goal.state.push_back(game->getState()[j][i]);
            count++;
        }
    }

    return goal;
}

void clearBoard() {
#if defined _WIN32
    system("cls");
#elif defined (__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
    system("clear");
#elif defined (__APPLE__)
    system("clear");
#endif
}

void onCanvasChanged(const std::string& canvas)
{
    clearBoard();
    ROS_INFO_STREAM(canvas);
}

/**
 * @brief Ход, пришедший от Ai
 * @param state состояние 
 * @param result ответ от Ai
 */
void aiMove(const actionlib::SimpleClientGoalState &state,
            const boost::shared_ptr<const tic_tac_toe::AiMoveResult> &result) {
    auto ok = game->aiMove(result->cageX, result->cageY);
    if(!ok)
        ROS_INFO("[BOARD]: AI move not done");
}

/**
 * @brief Ход, пришедший от пользователя
 * 
 * @param req ход
 * @param res not used
 * @return true always
 */
bool playerMove(tic_tac_toe::PlayerMove::Request &req,
                tic_tac_toe::PlayerMove::Response &res) {
    auto cageValid = [](int cage){
        return cage >=0 && cage < game->getBoardSize();
    };
    if(!cageValid(req.cageX) || !cageValid(req.cageY)){
        ROS_INFO("[BOARD]: Player input is not correct: X=%d Y=%d", req.cageX, req.cageY);
        return false;
    }

    auto ok = game->playerMove(req.cageX, req.cageY);
    if(!ok)
    {
        ROS_INFO("[BOARD]: Player move not done");
        return false;
    }

    if(!game->isGameOver() && !game->isLastStep())
    {
        ROS_INFO("[BOARD]: Start Ai task");
        aiMoveClient->sendGoal(createAiGoal(), aiMove);
    }

    return true;
}

/**
 * @brief Запуск новой игры
 * 
 * @param req размер полотна
 * @param res 
 * @return true always
 */
bool gameRestart(tic_tac_toe::Restart::Request &req,
                 tic_tac_toe::Restart::Response &res) {
    game = new tic_tac_toe::Game(onCanvasChanged);
    return true;
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "tic_tac_toe_board");
    ros::NodeHandle n;

    // считается, что Ai может думать и долго, поэтому его задача выполняется асинхронно
    aiMoveClient = new actionlib::SimpleActionClient<tic_tac_toe::AiMoveAction>("ai_move", false);
    game = new tic_tac_toe::Game(onCanvasChanged);

    ros::ServiceServer playerMoveService = n.advertiseService("player_move", playerMove);
    ros::ServiceServer restartService = n.advertiseService("restart", gameRestart);

    ros::spin();

    return 0;
}