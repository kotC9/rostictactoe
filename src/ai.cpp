#include "ros/ros.h"
#include "tic_tac_toe/AiMoveAction.h"
#include <actionlib/server/simple_action_server.h>

#include "AiAlgo.h"

actionlib::SimpleActionServer<tic_tac_toe::AiMoveAction> *server;
MinimaxAiAlgo algo;

void aiMove(const tic_tac_toe::AiMoveGoalConstPtr &goal) {
    auto boardSize = (uint8_t) sqrt((double) goal->state.size());
    std::vector<std::vector<tic_tac_toe::cageState>> state(boardSize, std::vector<tic_tac_toe::cageState>(boardSize));
    auto asCageState = [](uint8_t value) {
        if (value == tic_tac_toe::X)
            return tic_tac_toe::aiCage;
        else if (value == tic_tac_toe::O)
            return tic_tac_toe::playerCage;
        return tic_tac_toe::empty;
    };

    for (auto i = 0; i < goal->state.size(); i++) {
        auto y = (int) (1.0 * i / boardSize);
        auto x = i - y * boardSize;
        state[x][y] = asCageState(goal->state[i]);
    }

    ROS_INFO("[AI]: Start Algorithm");
    server->setSucceeded(algo.AiChooseCage(state));
}

/**
 * @brief Ai, ходит после пользователя
 */
int main(int argc, char **argv) {
    ros::init(argc, argv, "tic_tac_toe_ai");
    ros::NodeHandle n;
    server = new actionlib::SimpleActionServer<tic_tac_toe::AiMoveAction>(n, "ai_move", aiMove, false);
    server->start();
    ros::spin();

    return 0;
}