#include "ros/ros.h"
#include "tic_tac_toe/PlayerMove.h"
#include <cstdlib>

bool isNumber(const char* string) {
    unsigned long string_len = strlen(string);
    for(int i = 0; i < string_len; ++i) {
        if(!isdigit(string[i]))
            return false;
    }
    return true;
}

/**
 * @brief сервис, выполняющий ход игрока
 * 
 * @param argv argv[1] - координата по X, argv[2] - координата по Y, 
 */
int main(int argc, char **argv) {
    ros::init(argc, argv, "tic_tac_toe_player");
    if (argc != 3) {
        ROS_INFO("usage: tic_tac_toe player X Y");
        return 1;
    }
    if(!isNumber(argv[1]) || !isNumber(argv[2]))
    {
        ROS_INFO("X and Y must be digits");
        return 1;
    }

    tic_tac_toe::PlayerMove srv;
    srv.request.cageX = (uint32_t) atoi(argv[1]);
    srv.request.cageY = (uint32_t) atoi(argv[2]);

    ros::NodeHandle n;
    auto playerMoveClient = n.serviceClient<tic_tac_toe::PlayerMove>("player_move");
    if (playerMoveClient.call(srv)) {
        ROS_INFO("[PLAYER]: The move is made");
    } else {
        ROS_ERROR("[PLAYER]: Failed to call service player_move");
        return 1;
    }

    return 0;
}