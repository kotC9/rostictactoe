#include <vector>
#include "string"
#include <map>
#include "gameUtils.h"
namespace tic_tac_toe {


    /**
     * @brief Класс игры в Крестики-нолики, возможна игра на полотне до 255x255 (в теории)
     * 
     */
    class Game {

    private:
        int boardSize = 3;
        int maximumSteps = 3;

        std::map<cageState, std::string> winnerName = {
                {empty, "No one"},
                {O,     "Player"},
                {X,     "Ai"}
        };
        std::map<cageState, std::string> readableCageState = {
                {empty, "  "},
                {O,     "O"},
                {X,     "X"}
        };
        // [x][y]
        /**
         *  0 │ 1 │ 2
         * ───────────
         *  3 │ 4 │ 5
         * ───────────
         *  6 │ 7 │ 8
         */
        std::vector<std::vector<cageState>> state;
        std::string gameCanvas = "\n";

        int currentStep = 0;
        bool gameOver = false;
        cageState gameWinner{};
        void(*canvasChanged)(const std::string& canvas);
        /**
         * @brief Проверка, можно ли закончить игру
         *
         * @return cageState победитель. ВАЖНО: empty - можно закончить только при currentStep >= maximumSteps
         */
        cageState checkGameOver();

        /**
         * @brief отрисовка полотна игры в gameCanvas
         *
         */
        void redraw();

        /**
         * @brief Попытка записать ход игрока в x, y
         */
        bool tryMakeMove(uint32_t x, uint32_t y, cageState owner);

        void finishGame(cageState winner);

        bool doMove(uint32_t x, uint32_t y, cageState owner);
    public:

        explicit Game(void(*canvasChanged)(const std::string& canvas));

        std::vector<std::vector<cageState>> getState() { return state; }

        cageState getWinner() {return gameWinner;}

        bool isGameOver() {return gameOver;}

        bool isLastStep() { return currentStep >= maximumSteps;}

        int getBoardSize() { return boardSize;}

        /**
         * @brief Запись хода ИИ и подсчет игры
         *
         * @param x координата по x
         * @param y координата по y
         */
        bool aiMove(uint32_t x, uint32_t y);

        /**
         * @brief Запись хода пользователя и подсчет игры
         *
         * @param x координата по x
         * @param y координата по y
         * @return true удалось сделать ход
         * @return false не удалось сделать ход
         */
        bool playerMove(uint32_t x, uint32_t y);
    };
}
