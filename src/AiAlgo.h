#ifndef SRC_AIALGO_H
#define SRC_AIALGO_H

#include <tic_tac_toe/AiMoveResult.h>
#include "gameUtils.h"

class BaseAiAlgo{
public:
    virtual tic_tac_toe::AiMoveResult AiChooseCage(std::vector<std::vector<tic_tac_toe::cageState>> state){
        tic_tac_toe::AiMoveResult result;
        result.cageX = -1;
        result.cageY = -1;
        return result;
    };
};

class MinimaxAiAlgo {
private:
    bool isMovesLeft(std::vector<std::vector<tic_tac_toe::cageState>> board);
    int evaluate(std::vector<std::vector<tic_tac_toe::cageState>> b);
    int minimax(std::vector<std::vector<tic_tac_toe::cageState>> board, int depth, bool isMax);
public:
    tic_tac_toe::AiMoveResult AiChooseCage(std::vector<std::vector<tic_tac_toe::cageState>> state);
};
#endif //SRC_AIALGO_H
